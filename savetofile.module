<?php
function savetofile_init() {
  //drupal_add_js(drupal_get_path('module','savetofile') . '/savetofile.js');
}

/**
 * Implementation of hook_menu()
 */
function savetofile_menu($may_cache) {
  
  drupal_add_css(drupal_get_path('module','savetofile') . '/savetofile.css');
  
  $items = array();   
  
  // Sets page for table refresh
  $items[] = array(
    'path' => 'savetofile/save',
    'callback' => 'savetofile_save',
    'access' => user_access('savetofile use'),
    'type' => MENU_CALLBACK,
  );
  // Settings form
  $items[]= array(
    'path' => 'admin/settings/savetofile',
    'title' => t('Save To File Settings'),
    'callback' => 'drupal_get_form',
    'callback arguments' => 'savetofile_admin_settings',
    'access' => user_access('savetofile administer'),
    'description' => t('Global configuration of Save To File functionality.'),
    'type' => MENU_NORMAL_ITEM,
  ); 
  
  return $items;
}

/**
 * Implementation of hook_perm()
 */
function savetofile_perm() {
  return array("savetofile administer", "savetofile use");
}


/**
 * Defines admin settings form
 */
function savetofile_admin_settings() { 
  $form['savetofile_dir']= array(
    '#title' => 'Page directory',
    '#description' => 'The directory in the files directory where you want to save the files. If set to "saved_content", the directory will be "[files_directory]/saved_content".',
    '#type' => 'textfield',
    '#size' => '20',          
    '#default_value' => variable_get('savetofile_dir', 'saved_content'),
  );
  $form['savetofile_no_delete']= array(
    '#title' => 'Do not delete files',
    '#description' => 'After loading a saved file and saving the node, the file is deleted. If you would prefer to have keep the saved file as a backup and not delete it, check this.',
    '#type' => 'checkbox',
    '#default_value' => variable_get('savetofile_no_delete', ''),
  );
  $form['savetofile_suggest_filename']= array(
    '#title' => 'Enable suggested filename',
    '#description' => 'If checked, Save-to-File will suggest a filename based on the page URL or the node alias.',
    '#type' => 'checkbox',
    '#default_value' => variable_get('savetofile_no_delete', ''),
  );
  
  $form['savetofile_visibility'] = array(
    '#type' => 'fieldset',
    '#title' => t('Visibility'),
    '#collapsible' => FALSE,
    '#collapsed' => TRUE
  );

  $access = user_access('use PHP for block visibility');

  // If the visibility is set to PHP mode but the user doesn't have this block permission, don't allow them to edit nor see this PHP code
  if ($edit->settings['access'] == 2 && !$access) {
    $form['savetofile_visibility'] = array();
    $form['savetofile_visibility']['access'] = array(
      '#type' => 'value',
      '#value' => 2
    );
    $form['savetofile_visibility']['access_pages'] = array(
      '#type' => 'value',
      '#value' => $edit->settings['access_pages']
    );
  }
  else {
    $options = array(t('Show on every page except the listed pages.'), t('Show on only the listed pages.'));
    $description = t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are '!blog' for the blog page and !blog-wildcard for every personal blog. !front is the front page.", array('!blog' => theme('placeholder', 'blog'), '!blog-wildcard' =>  theme('placeholder', 'blog/*'), '!front' => theme('placeholder', '<front>')));

    if ($access) {
      $options[] = t('Show if the following PHP code returns <code>TRUE</code> (PHP-mode, experts only).');
      $description .= t('If the PHP-mode is chosen, enter PHP code between !php. Note that executing incorrect PHP-code can break your Drupal site.', array('!php' => theme('placeholder', '<?php ?>')));
    }
    $form['savetofile_visibility']['savetofile_access'] = array(
      '#type' => 'radios',
      '#title' => t('Show Save-to-File on specific pages'),
      '#default_value' => variable_get('savetofile_access','1'),
      '#options' => $options
    );
    $form['savetofile_visibility']['savetofile_access_pages'] = array(
      '#type' => 'textarea',
      '#title' => t('Pages'),
      '#default_value' => variable_get('savetofile_access_pages','node/*'),
      '#description' => $description
    );
  }
  return system_settings_form($form);
}

function savetofile_footer() {
  if (!user_access('savetofile use')) {
    return;
  }
  if (!file_exists(file_directory_path())) {
    drupal_set_message('In order to use Save-to-File, you will need to set up your file directory. '. l('Click here to set file settings','admin/settings/file-system') .'.', 'error');
  }
  if ($edit->settings['access'] < 2) {
    $path = drupal_get_path_alias($_GET['q']);
    $regexp = '/^('. preg_replace(array('/(\r\n?|\n)/', '/\\\\\*/', '/(^|\|)\\\\<front\\\\>($|\|)/'), array('|', '.*', '\1'. preg_quote(variable_get('site_frontpage', 'node'), '/') .'\2'), preg_quote(variable_get('savetofile_access_pages','node/*'), '/')) .')$/';
    $page_match = !(variable_get('savetofile_access','1') xor preg_match($regexp, $path));
  } 
  else {
    $page_match = drupal_eval(variable_get('savetofile_access_pages',''));
  }
  $path = '';
  if (variable_get('savetofile_suggest_filename','') == 1) {
    if (arg(0) == 'node' && arg(2) == 'edit') {
      $node = node_load(arg(1));
      $path = str_replace('/','_',$node->path) . '-n'. arg(1);
    } 
    else {
      $path = str_replace('/','_',trim($_GET['q'],'/'));
    }
  }
  if ($page_match) {
    $js = '
    $(document).ready(function test() {
      var nodeid = "'. arg(1) .'";
      $("textarea").each( function() {
        var tid = $(this).attr("id");
        var match = new Array();
        if (match = /savetofile_get\(\'(.*)\'\)/.exec($(this).val())) {
          var input_value = match[1];
        } else { 
          var input_value = "'. $path .'";
        }
        
        var savetofile_load_div = "<div class=\"savetofile_overall\"><div class=\"savetofile_control\" id=\"savetofile_control" + tid + "\"><a href=\"javascript:return false;\" onclick=\"savetofile_load(\'" + tid + "\',$(\'#" + tid + "\').val());return false;\">Load from file</a></div> <div class=\"savetofile_form\" id=\"savetofile_name_div" + tid + "\"> | Filename: <input type=\"text\" id=\"savetofile_name" + tid + "\" value=\""+ input_value +"\"></input></div><div style=\"display:none\" id=\"savetofile_delete" + tid + "\"></div></div>";
        var savetofile_save_div = "<div class=\"savetofile_overall\"><div class=\"savetofile_control\" id=\"savetofile_control" + tid + "\"><a href=\"javascript:return false;\" onclick=\"savetofile_save(\'" + tid + "\',$(\'#" + tid + "\').val());return false;\">Save to file</a></div> <div class=\"savetofile_form\" id=\"savetofile_name_div" + tid + "\"> | Filename:  <input type=\"text\" id=\"savetofile_name" + tid + "\" value=\""+ input_value +"\"></input></div><div style=\"display:none\" id=\"savetofile_delete" + tid + "\"></div></div>";
        
        
        if (/savetofile_get/.test($(this).val())) {
          //$(this).attr("disabled","disabled");
          $(this).after("" + savetofile_load_div + "");
          $("#savetofile_name_div" + $(this).attr("id")).attr("class","savetofile_hide");
        } else {
          $(this).after("" + savetofile_save_div + "");
        }
        //alert(savetofile_save_div);
      });
    });
    
    function savetofile_load(input_id,content) {
      //alert("test");
      $.ajax({
        url: "'. url('savetofile/save') .'",
        type: "POST", 
        data: "content=" + content + "&action=load&nid='. arg(1) .'&filename=" + $("#savetofile_name" + input_id).val(),
        error: function(){
            alert("There was a problem saving your content.");
        },
        success: function(msg){
          var funct_value = $("#savetofile_name" + input_id).val();
          $("#savetofile_control" + input_id).html("<a href=\"javascript:return false;\" onclick=\"savetofile_save(\'" + input_id + "\',$(\'#" + input_id + "\').val());return false;\">Save to file</a>");
          $("#" +input_id).val(msg);
          $("#" +input_id).html("<span>" + msg + "</span>");
          //$("#" +input_id).attr("disabled","");
          $("#savetofile_name_div" + input_id).attr("class","savetofile_form");
          $("#savetofile_delete" + input_id).html("<input type=\"text\" name=\"savetofile_delete\" value=\"" + $("#savetofile_name" + input_id).val() + "\"></input>");
        }
      });
    }
    
    function savetofile_save(input_id,content) {
      //alert("test");
      if ($("#savetofile_name" + input_id).val() == "") {
        alert("Please specify a file name in order to save your content");
        return false;
      }
      $.ajax({
        url: "'. url('savetofile/save') .'",
        type: "POST", 
        data: "content=" + content + "&action=save&nid='. arg(1) .'&filename=" + $("#savetofile_name" + input_id).val(),
        error: function(){
            alert("There was a problem saving your content.");
        },
        success: function(msg){
          var funct_value = $("#savetofile_name" + input_id).val();
          $("#savetofile_name_div" + input_id).attr("class","savetofile_hide");
          $("#" +input_id).val("<?php print savetofile_get(\'" + funct_value + "\');?>");
          $("#" +input_id).html("<span>&lt;?php print savetofile_get(\'" + funct_value + "\');?&gt;</span>");
          //$("#" +input_id).attr("disabled","disabled");
          $("#savetofile_control" + input_id).html("<a href=\"javascript:return false;\" onclick=\"savetofile_load(\'" + input_id + "\',$(\'#" + input_id + "\').val());return false;\">Load from file</a>");
          $("#savetofile_delete" + input_id).html("<span></span>");
        }
      });
    }';
  return '<script>'. $js .'</script>';
  }
}

function savetofile_nodeapi() {
  
}

function savetofile_form_alter($form_id, &$form) {
  if (variable_get('savetofile_no_delete','') != '1') {
    $dir = variable_get('savetofile_dir','saved_content');
    if (strpos($form_id,'_node_form')) {
      if ($_POST['savetofile_delete'] != '') {
        @unlink(file_directory_path() .'/'. $dir .'/'. $_POST['savetofile_delete'] . '.php');
      }
    }
  }
}

function savetofile_save() {
  $dir = variable_get('savetofile_dir','saved_content');
  if (!file_exists(file_directory_path() .'/'. $dir)) {
    mkdir(file_directory_path() .'/'. $dir);
  }
  $filename = file_directory_path() .'/' . $dir .'/'. $_POST['filename'] . '.php';
  if ($_POST['action'] == 'save') {
    $fp = fopen($filename,'w+');
    fwrite($fp,$_POST['content']);
    fclose($fp);
  }
  if ($_POST['action'] == 'load') {
    if (file_exists($filename)) {
      $fp = fopen($filename,'r');
      echo fread($fp,1000000);
      fclose($fp);
    }
  }
}

function savetofile_get($filename) {
  $dir = variable_get('savetofile_dir','saved_content');
  ob_start();
  include($_SERVER['DOCUMENT_ROOT'] .'/'. file_directory_path() .'/'. $dir  .'/' . $filename . '.php');
  $contents = ob_get_contents();
  ob_end_clean();
  return $contents;
}