
Save To File module:
-------------------------
Author - Chris Shattuck (www.impliedbydesign.com)
License - GPL


Overview:
-------------------------
The Save To File module allows a user to save the content of node textbox to a file, for
easier editing or for backing up complicated pages.


Installation
-------------------------
To install:
- Download the module
- Add the files to your module directory
- Enable the module

To use:
- To save the content of a textarea to a file, click the "Save to file" link at the bottom
of the textarea
- To load it back into the text area, click the "Load from file" link.

Tips:
- When saving the text to a file, the content is replaced with a PHP function. This means
that in order to display the content, the textarea needs to be able to process PHP. For
a node form, this means enabling the PHP input filter. Other textareas vary in how they
process content.
- Save-to-File is enabled automatically for anything matching node/*. To enable it for other
textareas, you will need to change the Save-to-file settings at admin/settings/savetofile.
- You can save and load content on a page several times without saving the page itself.


Last updated:
------------
